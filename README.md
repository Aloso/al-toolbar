# Toolbar

A html/css/js toolbar that supports horizontal scrolling.

![Toolbar](Toolbar.png)

> Note: This plugin is still in an early stage of development. Please wait until I shuffled out most of the bugs.

## Main features

- Shows two scroll buttons when scrolling is required
- Keyboard users can focus the toolbar with the <kbd>Tab</kbd> key and scroll with the <kbd>◀</kbd> <kbd>▶</kbd> arrow keys
- Scroll buttons are hidden on smaller touch devices (e.g. smartphones, tablets)
- When the toolbar is scrolled to the limit in one direction, the corresponding button is grayed out
- Fixed buttons can be attached on the left and right
- Dark theme
- Combined file size: ≈ 4 KB when minified and gzipped
- No jQuery required!

## Implementation notes

When implementing this yourself, you may want to use `display: hidden` to hide the horizontal scrollbar. However, this disables the native scrolling behaviour, so scrolling with middle-click or two-finger-scrolling on a touchpad is no longer possible.

Instead, this plugin hides the scrollbar by putting a Toolbar `<div>` inside a smaller `<div>` with `overflow: hidden`. The layout uses `display: table`.

## Compatibility

Most features should work even in Internet Explorer 8. The optional `ResizeObserver` integration currently only works in the latest Chrome, but can be used in any browser with a [polyfill](https://github.com/que-etc/resize-observer-polyfill).

## A simple example

Include the stylesheet and the javascript in the `<head>` of your html document. Make sure to include the compiled and minified script, not the development version.

You probably also want to include a `ResizeObserver` polyfill:

```html
<link href="al-style.css" rel="stylesheet">

<script src="al-toolbar.min.js"></script>
<script src="ResizeObserver.min.js"></script>
```

The simplest usage utilizes _custom html elements_:

```html
<al-toolbar>
    <al-content>
        <div class="al-tab">Hello,</div>
        <div class="al-tab">World!</div>
        <div class="al-tab">This is a rather long text. Will it fit?</div>
    </al-content>
    <button>Extra button</button>
</al-toolbar>
```

To initialize the toolbar, just call

```javascript
Toolbar.init();
```

This looks for `<al-toolbar>` nodes in the document and initializes them.

## Configuration

### Content

The content is specified using **html elements**. An `<al-toolbar>` element may contain the following elements:

- `<al-content>`: This element contains the Toolbar content of the toolbar.
- `<al-left-arrow>`/`<al-right-arrow>`: These elements contain the content of the scroll buttons. Default content is `◀` / `▶`.
- Anything else: Other elements are attached to the toolbar, in the order they appear in the document.
  
  Make sure that these elements support `display: table-cell`. For example, `<button>` is not allowed here. Instead, use `<div role="button">`.

### Options

Options are specified using **html attributes** of `<al-toolbar>`. The following options are supported:

- `al-height`: an integer that specifies the height (and line height) of the toolbar in CSS pixels.
  
  - Default is `30`
  
- `al-buttons`: Specifies when the scroll buttons are visible.
  Supported values: `"never"`, `"always"`, `"auto"`.
  
  When the value is `"auto"`, scroll buttons are only visible when the content overflows.
  
  - Default is `"auto"`
  
- `al-buttons-touch`: This attribute overrides `al-buttons` on smaller touch devices (e.g. smartphones, tablets).
  
  By default, no scroll buttons are shown because these devices usually support finger scrolling and don't have a mouse attached.
  
  - Default is `"never"`

- `al-arrows-focusable`: Specifies whether the scroll buttons can be focused with the keyboard. Supported values: `"true"`, `"false"`
  
  This is usually not required because the toolbar can be scrolled with the <kbd>◀</kbd> <kbd>▶</kbd> arrow keys.
  
  - Default is `"false"`

- `al-scroll-distance`: The amount of pixels scrolled when a scroll button is clicked.
  
  - Default is `120`

- `al-scroll-duration`: The amount of milliseconds the scroll animation takes.
  
  - Default is `400`

- `al-scroll-timing`: Specifies the scroll animation timing function. Supported values are `"linear"` (constant animation speed) and `"ease"` (animation fades in and out smoothly).
  
  - Default is `"ease"`

### Dark theme

To use the dark theme, simply add the CSS class `"dark-theme"` to the toolbar's parent node or any of its ancestors (e.g. the `<body>`).

![Dark theme](Toolbar_dark.png)

### Borderless mode

To remove the default, rounded border, add the CSS class `"al-no-border"` to the toolbar:

```html
<al-toolbar class="al-no-border">
    <al-content>...</al-content>
    ...
</al-toolbar>
```

## Updates

By default, the scroll buttons are only shown when the content overflows. Therefore, `Toolbar.prototype.update()` needs to be called every time the buttons need to be hidden or shown.

However, there is no simple way to listen to dimension changes, and the `ResizeObserver` API isn't supported in most browsers.

There are two ways around this:

1. Manually call `Toolbar.prototype.update()` every time the toolbar is resized or its content is changed.
    
   However, this is annoying and rather error prone.
    
2. Include a [polyfill](https://github.com/que-etc/resize-observer-polyfill) for the `ResizeObserver`, and `Toolbar.init()` will take care of everything.

   If your toolbar is created dynamically, you can also attach the resize observer manually:
   
   ```javascript
   my_toolbar.observe_with(Toolbar.resize_observer());
   ```

## Pure JS usage

If you create your toolbar dynamically, you can do so with pure javascript, without the markup.

    //TODO: Show code example

## The `Toolbar` class

The `Toolbar` class is a wrapper for one toolbar.

### Static methods

- `Toolbar.constructor(node: HTMLElement, contentNode: HTMLElement, options)`
  
  Creates a toolbar. **It's discouraged to use the constructor directly**. Instead, use `Toolbar.create(options)`.

- `Toolbar.create(options)`
  
  Warning: This feature is unstable.
  
  Creates a toolbar. Supported options are:
  
  - content: `string | Node | Node[] | JQuery` – html content of the scroll pane
  - height: `int` – height of the element in pixels. _Default: 30_
  - left_arrow: `string` – text or HTML inside the right scroll arrow button.
    
    Default: `"◀"`
  - right_arrow: `string` – text or HTML inside the left scroll arrow button.
    
    Default: `"▶"`
  - left_controls: `HTMLElement[]` – array containing controls at the left of the scroll pane
  - right_controls: `HTMLElement[]` - array containing controls at the right of the scroll pane
  - buttons: `string` – defines when scroll arrows are shown on non-touchscreens.
    
    Can be `"never"`, `"auto"` or `"always"`. Default: `"auto"`
  - buttons_touch: `string` – defines when scroll arrows are shown on touch screens.
    
    Can be `"never"`, `"auto"` or `"always"`. Default: `"never"`
  - arrows_focusable: `boolean` – whether the scroll arrows can be focused with the keyboard.
    
    Default: `false`
  - scroll_distance: `int` – amount of pixels scrolled when a scroll button is clicked.
    
    Default: 120
  - scroll_duration: `int` - amount of milliseconds the scroll animation takes
    
    Default: 400
  - scroll_timing: `string` - The timing function for the animation. By default, the animation fades in and out smoothly.
    
    Can be `"ease"` or `"linear"`. Default: `"ease"`

- `Toolbar.from(node: HTMLElement): Toolbar`
  
  Creates a toolbar. `node` may contain html attributes and children, as described in the [Configuration](#Configuration) section

- `Toolbar.init()`
  
  Searches the document for `<al-toolbar>` elements and initializes them using `Toolbar.from(HTMLElement)`.

- `Toolbar.resize_observer()`
  
  If a `ResizeObserver` class is found in the `window` object, a new resize observer for updating toolbars is returned. Otherwise, `null` is returned.
  
  If called multiple times, this function always returns the same resize observer.

### Instance methods

- `Toolbar.prototype.update()`
  
  Updates the layout. This method needs to be called every time a scroll button has to be enabled, disabled, hidden or made visible again.

  It is recommended to use a `ResizeObserver` for this, see above (section [Updates](#Updates)).

- `Toolbar.prototype.observe_with(resize_observer: ResizeObserver)`
  
  Observes dimension changes of this toolbar and its scroll pane with the given resize observer.

- `Toolbar.prototype.node()`
  
  Returns the outermost HTML element of the toolbar.

- `Toolbar.prototype.content()`
  
  Returns the HTML element that contains the scroll pane of the toolbar.

- `Toolbar.prototype.scroll_node()`
  
  Returns the scroll pane, that is, the Toolbar HTML element of the toolbar.

- `Toolbar.prototype.left_btn()`
  
  Returns the left scroll button. This button always exists, even if it is hidden (using `display: none`)
  
- `Toolbar.prototype.right_btn()`
  
  Returns the right scroll button. This button always exists, even if it is hidden (using `display: none`)

- `Toolbar.prototype.node_inner()`
  
  Returns the HTML element that directly contains the scroll buttons, the content and the attached buttons. This node uses `display: table` for layout.

## License

Licensed with MIT license
(function(global) {
    "use strict";
    
    const logger = str => console.log(str);
    
    const is_touch = 'ontouchstart' in window || navigator.maxTouchPoints;

    const timing = {
        linear: x => x,
        ease:   x => x * (x * (x * (x * (10 - x * 2.5) - 14) + 7) + 0.5),
    };
    
    /**
     * @param {HTMLElement} node
     * @param {string|string[]} events
     * @param {function(Event)|(function(Event))[]} callbacks
     * @return {HTMLElement[]}
     */
    function events(node, events, callbacks) {
        if (typeof events === "string") events = events.split(" ");
        if (!(callbacks instanceof Array)) callbacks = [callbacks];
        
        for (let i = 0; i < events.length; i++) {
            for (let ii = 0; ii < callbacks.length; ii++) {
                node.addEventListener(events[i], callbacks[ii]);
            }
        }
        return node;
    }
    
    function get_parent(node, parent_filter) {
        while (!parent_filter(node) && node !== document.rootElement) {
            node = node.parentNode;
        }
        return node;
    }
    
    /**
     * A class for a toolbar that can be scrolled horizontally.
     */
    class Toolbar {
        /**
         * @param {HTMLElement} node
         * @param {HTMLElement} contentNode
         * @param {object} options
         *
         * @param {int}           options.height            - height of the element in pixels
         * @param {string}        options.left_arrow        - text or HTML inside the right scroll arrow button
         * @param {string}        options.right_arrow       - text or HTML inside the left scroll arrow button
         * @param {HTMLElement[]} options.left_controls     - array containing controls at the left of the scroll pane.
         * @param {HTMLElement[]} options.right_controls    - array containing controls at the right of the scroll pane.
         * @param {string}        options.buttons           - defines when scroll arrows are shown on non-touchscreens.
         *                                                      Can be "never", "auto" or "always".
         * @param {string}        options.buttons_touch     - defines when scroll arrows are shown on touchscreens.
         *                                                      Can be "never", "auto" or "always".
         * @param {boolean}       options.arrows_focusable  - whether the scroll arrows can be focused with the keyboard
         * @param {int}           options.scroll_distance   - number of pixels scrolled
         * @param {int}           options.scroll_duration   - number of milliseconds the animation takes
         * @param {string}        options.scroll_timing     - either "ease" (default) or "linear"
         */
        constructor(node, contentNode, options) {
            this._node = node;
            node.al_toolbar = this;
            
            this.options = options;
            
            // add class name(s)
            if (node.className !== "") node.className += " ";
            node.className += "al-h-scroll-wrapper";
            if (is_touch) node.className += " al-touch";
            node.className += " al-s-" + options.buttons + " al-t-" + options.buttons_touch;
            
            // set height, reset innerHTML
            let height = options.height || 24;
            node.style.height = node.style.lineHeight = height + "px";
            node.innerHTML = "";
            
            // create node with (display: table)
            let table_node = document.createElement("div");
            table_node.className = "al-h-scrollable al-can-scroll";
            
            // add left controls
            /** @type {HTMLElement[]} */
            let left_controls = options.left_controls || [];
            for (let i = 0; i < left_controls.length; i++) {
                let cell = left_controls[i];
                if (cell.nodeName === "BUTTON") {
                    console.error("<button> elements cannot be attached!");
                }
                if (cell.getAttribute("role") === "button") {
                    cell.className += " al-u-button";
                    cell.tabIndex = 0;
                }
                cell.className += " al-cell al-attached-button";
                table_node.appendChild(cell);
            }
            
            // add left scroll button
            this._left = document.createElement("div");
            this._left.className = "al-cell al-u-button al-left-sb";
            this._left.innerHTML = options.left_arrow;
            if (options.arrows_focusable) {
                this._left.tabIndex = 0;
                this._left.setAttribute("role", "button");
            }
            table_node.appendChild(this._left);

            // add content, add scroll node to content
            this._scroll_node = contentNode || document.createElement("div");
            if (this._scroll_node.className !== "") this._scroll_node.className += " ";
            this._scroll_node.className += "al-scroll-content";
            this._scroll_node.tabIndex = 0;
            this._scroll_node.al_toolbar = this;
            
            this._content = document.createElement("div");
            this._content.className = "al-cell al-scroll";
            this._content.appendChild(this._scroll_node);
            
            table_node.appendChild(this._content);
            
            // add right scroll button
            this._right = document.createElement("div");
            this._right.className = "al-cell al-u-button al-right-sb";
            this._right.innerHTML = options.right_arrow;
            if (options.arrows_focusable) {
                this._right.tabIndex = 0;
                this._right.setAttribute("role", "button");
            }
            table_node.appendChild(this._right);
            
            // add right controls
            /** @type {HTMLElement[]} */
            let right_controls = options.right_controls || [];
            for (let i = 0; i < right_controls.length; i++) {
                let cell = right_controls[i];
                if (cell.nodeName === "BUTTON") {
                    console.error("<button> elements cannot be attached!");
                }
                if (cell.getAttribute("role") === "button") {
                    cell.className += " al-u-button";
                    cell.tabIndex = 0;
                }
                cell.className += " al-cell al-attached-button";
                table_node.appendChild(cell);
            }
            
            // add table node
            node.appendChild(table_node);
            this._table_node = table_node;
            
            // make interactive
            events(this._scroll_node, "scroll", () => this.update());
            events(this._node, "mouseover", () => this.update());
            events(this._scroll_node, "focus blur", e => {
                if (e.type === "focus") {
                    this._node.className += " al-focused";
                } else {
                    this._node.className = this._node.className.replace(/ ?\bal-focused\b ?/, " ");
                }
            });
            
            this._ali = null;
            
            events(this._left, "mousedown touchstart", () => {
                let distance = this.options.scroll_distance == null ? 120 : options.scroll_distance;
                this.scroll_to(-distance);

                if (!this.options.arrows_focusable) requestAnimationFrame(() => this._scroll_node.focus());
            });
            events(this._right, "mousedown touchstart", () => {
                let distance = this.options.scroll_distance == null ? 120 : options.scroll_distance;
                this.scroll_to(distance);

                if (!this.options.arrows_focusable) requestAnimationFrame(() => this._scroll_node.focus());
            });
            
            // update
            this.update();
        }
        
        /** @returns HTMLElement */
        node() {return this._node;}
        /** @returns HTMLElement */
        content() {return this._content;}
        /** @returns HTMLElement */
        scroll_node() {return this._scroll_node;}
        /** @returns HTMLElement */
        left_btn() {return this._left;}
        /** @returns HTMLElement */
        right_btn() {return this._right;}
        /** @returns HTMLElement */
        node_inner() {return this._table_node;}

        /** @param {number|HTMLElement} nop */
        scroll_to(nop) {
            if (this._ali != null) this._ali.cancel();
            
            let ni = this._scroll_node;
            let start = ni.scrollLeft;
            let end;
            let distance;
            if (typeof nop === "number") {
                end = start + nop;
                distance = Math.abs(nop);
            } else {
                let offset = Math.max(this._content.clientWidth - nop.clientWidth, 0) / 2;
                end = nop.offsetLeft - this._content.offsetLeft - (offset || 0);
                distance = this.options.scroll_distance || 120;
            }
            end = Math.min(ni.scrollWidth - ni.clientWidth, Math.max(0, end));
            
            let duration = this.options.scroll_duration == null ? 400 : this.options.scroll_duration;
            let timing_fn = this.options.scroll_timing === "linear" ? timing.linear : timing.ease;
            
            if (duration === 0) {
                ni.scrollLeft = end;
                return;
            }

            let final_distance = Math.abs(end - start);
            duration *= final_distance / distance;
            
            this._ali = new ALimation({ duration: duration, timing: timing_fn })
                .on_update(x => ni.scrollLeft = start + (end - start) * x)
                .start();
        }
        
        update() {
            let sn = this.scroll_node();
            let pn = this.node_inner();
            /** @type {string[]} */
            let classes = ["al-h-scrollable"];
            
            let scroll_l = sn.scrollLeft > 0;
            let scroll_r = sn.scrollLeft < sn.scrollWidth - sn.clientWidth - 1;
            if (scroll_r || scroll_l) {
                classes.push("al-can-scroll");
                
                if (scroll_l) {
                    classes.push("al-can-scroll-left");
                } else {
                    classes.push("al-no-scroll-left");
                }
                if (scroll_r) {
                    classes.push("al-can-scroll-right");
                } else {
                    classes.push("al-no-scroll-right");
                }
            } else {
                classes.push("al-no-scroll-left al-no-scroll-right");
            }
            pn.className = classes.join(" ");
        }
        
        observe_with(resize_observer) {
            if (resize_observer === null) {
                console.error("resize_observer is null. Forgot to add a ResizeObserver polyfill to your site?");
                return;
            }
            resize_observer.observe(this.node());
            resize_observer.observe(this.scroll_node());
            return this;
        }
    }
    
    /**
     * @param {HTMLElement} node
     * @param obj
     * @param {Array} attrs
     * @param {Array} defaults
     */
    function parseAttributes(node, obj, attrs, defaults) {
        for (let i = 0; i < attrs.length; i++) {
            let attr = node.getAttribute("al-" + attrs[i]);
            let def = defaults[i];
            if (attr === null) {
                attr = def;
            } else if (typeof def === "boolean") {
                attr = attr === "true"
            } else if (typeof def === "number") {
                attr = +attr;
            }
            obj[attrs[i].replace("-", "_")] = attr;
        }
        return obj;
    }
    
    /**
     * @param {HTMLElement} node
     * @return {Toolbar}
     */
    Toolbar.from = function (node) {
        
        let la = "<span style='font-size:80%; line-height: 0.1em; vertical-align: 0.18em;'>◀</span>";
        let ra = "<span style='font-size:80%; line-height: 0.1em; vertical-align: 0.18em;'>▶</span>";
        
        let attrs = "height buttons buttons-touch left-arrow right-arrow arrows-focusable scroll-distance scroll-duration scroll-timing ".split(" ");
        let defaults = [30, "auto",     "never",    la,         ra,         false,              120,            400,        "ease"];
        
        let obj = parseAttributes(node, {
            left_controls: [],
            right_controls: [],
        }, attrs, defaults);
        
        attrs.forEach(attr => node.removeAttribute("al-" + attr));
        
        let content = null;
        for (let i = 0; i < node.children.length; i++) {
            let n = node.children[i];
            if (n.nodeName === "AL-CONTENT") {
                content = n;
            } else if (n.nodeName === "AL-LEFT-ARROW") {
                obj.left_arrow = n.innerHTML;
            } else if (n.nodeName === "AL-RIGHT-ARROW") {
                obj.right_arrow = n.innerHTML;
            } else if (content) {
                obj.right_controls.push(n);
            } else {
                obj.right_controls.push(n);
            }
        }
        if (content == null) throw new Error("no <al-content> node found");
        
        return new Toolbar(node, content, obj);
    };

    /**
     * @param {object} options
     * 
     * @param {string|Node|Node[]|{each:function(Node)}} options.content - html content of the scroll pane
     * 
     * @param {int}           options.height            - height of the element in pixels
     * @param {string}        options.left_arrow        - text or HTML inside the right scroll arrow button
     * @param {string}        options.right_arrow       - text or HTML inside the left scroll arrow button
     * @param {HTMLElement[]} options.left_controls     - array containing controls at the left of the scroll pane.
     * @param {HTMLElement[]} options.right_controls    - array containing controls at the right of the scroll pane.
     * @param {string}        options.buttons           - defines when scroll arrows are shown on non-touchscreens.
     *                                                      Can be "never", "auto" or "always".
     * @param {string}        options.buttons_touch     - defines when scroll arrows are shown on touchscreens.
     *                                                      Can be "never", "auto" or "always".
     * @param {boolean}       options.arrows_focusable  - whether the scroll arrows can be focused with the keyboard
     * @param {int}           options.scroll_distance   - number of pixels scrolled
     * @param {int}           options.scroll_duration   - number of milliseconds the animation takes
     * @param {string}        options.scroll_timing     - either "ease" (default) or "linear"
     * @returns {Toolbar}
     */
    Toolbar.create = function(options) {
        let content = options.content || "";
        let node = document.createElement("div");
        if (typeof content === "string") {
            node.innerHTML = content;
        } else if (content instanceof Node) {
            node.appendChild(content);
        } else if (content instanceof Array) {
            content.forEach(c => node.appendChild(c));
        } else if ("each" in content) {
            content.each(c => node.appendChild(c));
        }
        //TODO defaults
        return new Toolbar(node, document.createElement("div"), options);
    };
    
    let ro = null;
    
    Toolbar.init = function() {
        /** @type {NodeListOf< HTMLElement | {al_toolbar: Toolbar} >} */
        const nodes = document.querySelectorAll("al-toolbar");
        const ro = Toolbar.resize_observer();
        
        nodes.forEach(element => {
            if (!element.al_toolbar) {
                Toolbar.from(element);
                if (ro !== null) element.al_toolbar.observe_with(ro);
            }
        });
    };
    
    /** @return {ResizeObserver} */
    Toolbar.resize_observer = function() {
        if (ro !== null || global.ResizeObserver) {
            if (ro === null) {
                logger("ResizeObserver added");
                ro = new ResizeObserver(elements => {
                    for (const element of elements) element.target.al_toolbar.update();
                });
            }
        } else {
            logger("No ResizeObserver available. Please consider adding a polyfill to your site.");
        }
        return ro;
    };
    
    global.Toolbar = Toolbar;
    
    
    // TODO check requestAnimationFrame browser support, update README or add polyfill if necessary
    
    
    class ALimation {
        /**
         * @param {object} options
         * @param {number?} options.duration
         * @param {(function(number):number)?} options.timing
         * @param {function?} options.on_start
         * @param {function?} options.on_update
         * @param {function?} options.on_finish
         * @param {function?} options.on_cancel
         * @param {(function|ALimation)?} options.then
         */
        constructor(options) {
            this.options = options;

            if (!options.duration) options.duration = ALimation.defaults.duration;
            if (!options.timing) options.timing = ALimation.defaults.timing;
            if (!options.on_update) options.on_update = ALimation.defaults.on_update;
            
            this.started = false;
            this.stopped = false;
            this.canceled = false;
            this.t = 0;
        }

        /**
         * Starts the animation.
         * If the animation is finished, it is started again.
         * If the animation has been cancelled, this does nothing.
         *
         * @return ALimation
         */
        start() {
            if (this.started || this.canceled) {
                return this;
            }
            this.stopped = false;
            
            this.started_at = +new Date();
            (this.options.on_start || ALimation.defaults.on_start)();
            
            let that = this;
            that.started = true;
            requestAnimationFrame(ali_frame);
            return this;

            function ali_frame() {
                if (that.canceled) {
                    that.started = false;
                    (that.options.on_cancel || ALimation.defaults.on_cancel)();
                } else {
                    let t = (+new Date() - that.started_at) / that.options.duration;
                    if (t >= 1) t = 1;

                    that.t = that.options.timing(t);
                    that.options.on_update(that.t);

                    if (t === 1) {
                        that.stopped = true;
                        that.started = false;
                        (that.options.on_finish || ALimation.defaults.on_finish)();

                        let then = that.options.then || ALimation.defaults.then;
                        if (then instanceof ALimation) then.start();
                        else then();
                    } else {
                        requestAnimationFrame(ali_frame);
                    }
                }
            }
        }
        /**
         * Cancels the animation.
         * Even if the animation is already finished, on_cancel() is called and chained animations are cancelled.
         *
         * @return ALimation
         */
        cancel() {
            if (this.canceled) return this;
            this.canceled = true;
            if (this.stopped && this.options.on_cancel) {
                this.options.on_cancel();

                let then = this.options.then || ALimation.defaults.then;
                if (then instanceof ALimation) then.cancel();
            }
            return this;
        }
        
        /**
         * @param {int} t new duration
         * @return ALimation
         */
        duration(t) {
            this.options.duration = t;
            return this;
        }
        /**
         * timing function. Should be either timing.linear or timing.ease.
         * @param {function(number): number} f
         * @return {ALimation}
         */
        timing(f) {
            this.options.timing = f;
            return this;
        }
        /**
         * @param {function()} f function called when the animation is started
         * @return ALimation
         */
        on_start(f) {
            this.options.on_start = f;
            return this;
        }
        /**
         * @param {function(number)} f function called every frame
         * @return ALimation
         */
        on_update(f) {
            this.options.on_update = f;
            return this;
        }
        /**
         * @param {function()} f function called when the animation is cancelled
         * @return ALimation
         */
        on_cancel(f) {
            this.options.on_cancel = f;
            return this;
        }
        /**
         * @param {function()} f function called when the animation is finished
         * @return ALimation
         */
        on_finish(f) {
            this.options.on_finish = f;
            return this;
        }
        
        /**
         * If af is an ALimation, it's chained to this animation and started as soon as this animation is finished.
         * If af is just a function, it is executed when this animation is finished.
         *
         * Note that cancel() also cancels all chained animations, even if the current animation is already finished.
         * This can be circumvented by passing a function that starts an animation.
         *
         * @param {ALimation|function()} af
         * @return ALimation
         */
        then(af) {
            this.options.then = af;
            return this;
        }
    }
    
    ALimation.defaults = {
        duration: 300,
        timing: timing.linear,
        on_start:  () => {},
        on_update: () => {},
        on_finish: () => {},
        on_cancel: () => {},
        then: () => {},
    };
    
})(window);
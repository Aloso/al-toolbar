(function() {
    "use strict";

    let is_dark_mode = false;
    
    let $toggle_dark_mode = document.querySelector("#toggle_dark_mode");
    
    if (document.location.hash === "") document.location.hash = "#d";
    if (document.location.hash.indexOf("#n") === 0) {
        toggle_dark_mode();
    }
    
    setTimeout(() => document.body.style.transition = "background-color .3s", 10);
    
    function toggle_dark_mode() {
        if (document.body.className.indexOf('dark-theme') !== -1) {
            document.body.className = document.body.className.replace(/\bdark-theme\b/, "");
            $toggle_dark_mode.innerHTML = "OFF";
            document.location.hash = "#d-" + document.location.hash.substr(3);
            is_dark_mode = false;
        } else {
            document.body.className += " dark-theme";
            $toggle_dark_mode.innerHTML = "ON";
            document.location.hash = "#n-" + document.location.hash.substr(3);
            is_dark_mode = true;
        }
    }

    /**
     * @param {NodeListOf<HTMLElement>|HTMLElement[]} links
     * @param {string} current_hash
     * @param {Window|HTMLElement} scroll_pane
     * @constructor
     */
    function AutoHeadings(links, current_hash, scroll_pane) {
        scroll_pane.addEventListener("scroll", () => this.update());
        
        this.current = -1;
        
        /** @type {HTMLElement[]} */
        this.links = [];
        links.forEach((/** HTMLElement */ l, /** int */ i) => {
            this.links.push(l);
            l.addEventListener("click", () => {
                let ch = this.headings[i];
                if (ch != null) ch.scrollIntoView();
                requestAnimationFrame(() => {
                    this.select(i);
                    
                    // noinspection JSUnresolvedVariable
                    /** @type {Toolbar} */
                    let tb = this.links[i].parentElement.parentElement.al_toolbar;
                    if (tb != null) tb.scroll_to(this.links[i].parentElement);
                });
            });
            if (l.getAttribute("data-href") === current_hash) this.current = i;
        });
        
        /** @type {HTMLElement[]} */
        this.headings = this.links.map(l => document.querySelector("#" + l.getAttribute("data-href")));
    }
    
    /** @return {HTMLElement} */
    AutoHeadings.prototype.current_link = function() {
        return this.links[this.current];
    };
    
    /** @return {HTMLElement} */
    AutoHeadings.prototype.current_heading = function() {
        return this.headings[this.current];
    };
    
    AutoHeadings.prototype.select = function(heading_id) {
        let curr = this.current_link();
        if (curr != null) curr.parentNode.className = curr.parentElement.className.replace(/\bactive\b/, "");
        
        this.current = heading_id;
        curr = this.current_link();
        if (curr == null) return;
        
        let href = curr.getAttribute("data-href");
        document.location.hash = is_dark_mode ? "#n-" : "#d-" + href;
        
        curr.parentNode.className += " active";
    };
    
    AutoHeadings.prototype.update = function() {
        let new_current = this.current;
        while (true) {
            let curr = this.headings[new_current];
            if (curr != null && curr.getBoundingClientRect().top > 80) {
                new_current--;
            } else {
                break;
            }
        }
        if (new_current === this.current) {
            while (true) {
                let next = this.headings[new_current + 1];
                if (next != null && next.getBoundingClientRect().top <= 80) {
                    new_current++;
                } else {
                    break;
                }
            }
        }
        if (this.current !== new_current) {
            this.select(new_current);
            if (this.current_link() != null) {
                // noinspection JSUnresolvedVariable
                /** @type {Toolbar} */
                let tb = this.current_link().parentElement.parentElement.al_toolbar;
                if (tb != null) tb.scroll_to(this.current_link().parentElement);
            }
        }
    };
    
    
    let auto_headings = new AutoHeadings(document.querySelectorAll("a[data-href]"),
        document.location.hash.substr(2),
        window);
    
    $toggle_dark_mode.addEventListener("click", toggle_dark_mode);
    
    Toolbar.init();
    
})();